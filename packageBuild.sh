#!/bin/bash
echo "Shell Start to generate package.xml ..."
metaItems=(`git diff --name-status 87b237b 45cb141`)
echo "Shell: generate empty deployment folder: manifest ..."
pageString=""
componentString=""
clsString=""
triggerString=""
auraComponentString=""
for i in ${!metaItems[@]}
do 
    metaName=${metaItems[i]}
    if [[ ${metaItems[i]} == *".page" ]] && [[ -f "$metaName" ]]; then
        pageName=${metaName#src/pages/}
        pageString=$pageString"<members>"${pageName%.page}"</members>"
        cp -p "$metaName" "manifest/pages"
        cp -p "$metaName-meta.xml" "manifest/pages"
    elif [[ ${metaItems[i]} == *".component" ]] && [[ -f "$metaName" ]]; then
        componentName=${metaName#src/components/}
        componentString=$componentString"<members>"${componentName%.component}"</members>"
        cp -p "$metaName" "manifest/components"
        cp -p "$metaName-meta.xml" "manifest/components"
    elif [[ ${metaItems[i]} == *".cls" ]] && [[ -f "$metaName" ]]; then 
        className=${metaName#src/classes/}
        clsString=$clsString"<members>"${className%.cls}"</members>"
        cp -p "$metaName" "manifest/classes"
        cp -p "$metaName-meta.xml" "manifest/classes"
    elif [[ ${metaItems[i]} == *".trigger" ]] && [[ -f "$metaName" ]]; then
        triggerName=${metaName#src/triggers/}
        triggerString=$triggerString"<members>"${triggerName%.trigger}"</members>"
        cp -p "$metaName" "manifest/triggers"
        cp -p "$metaName-meta.xml" "manifest/triggers"
	elif [[ ${metaItems[i]} == *".trigger" ]] && [[ -f "$metaName" ]]; then
        auraComponent=${metaName#src/AuraDefinitionBudle/}
        auraComponentString=$auraComponentString"<members>"${auraComponent%.cmp}"</members>"
        cp -p "$metaName" "manifest/aura"
        cp -p "$metaName-meta.xml" "manifest/aura"
    fi
done
if [ "$pageString" != "" ]; then
    pageString="<types>$pageString<name>ApexPage</name></types>"
fi
if [ "$componentString" != "" ]; then 
    componentString="<types>$componentString<name>ApexComponent</name></types>"
fi
if [ "$clsString" != "" ]; then 
    clsString="<types>$clsString<name>ApexClass</name></types>"
fi
if [ "$triggerString" != "" ]; then
    triggerString="<types>"$triggerString"<name>ApexTrigger</name></types>"
fi
if [ "$triggerString" != "" ]; then
    auraComponentString="<types>"$auraComponentString"<name>AuraDefinitionBudle</name></types>"
fi
packageString="<?xml version=\"1.0\" encoding=\"UTF-8\"?><Package xmlns=\"http://soap.sforce.com/2006/04/metadata\">$pageString$componentString$clsString$triggerString<version>36.0</version></Package>"
echo $packageString > manifest/package.xml
echo "Shell: package.xml is ready!"